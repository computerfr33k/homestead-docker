# DEPRECATED IN FAVOR OF [Laradock](https://github.com/LaraDock/laradock)

[![Build Status](https://travis-ci.org/computerfr33k/homestead-docker.svg?branch=master)](https://travis-ci.org/computerfr33k/homestead-docker)

# Laravel homestead-docker
Create a homestead docker container for your development env. ( files taken from laravel settler: provision.sh (modified) + serve.sh )

### Install docker && docker compose
please refer to these tutorials:
* install docker (https://docs.docker.com/installation/ubuntulinux/)
```shell 
curl -sSL https://get.docker.com/ | sh
```
* install docker compose (https://docs.docker.com/compose/install/)
```shell
curl -L https://github.com/docker/compose/releases/download/1.6.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
```

### Launch your containers
There are only two containers to run. web container ( includes everything except your database ), and mariadb container.
```shell
sudo docker-compose up -d
```
this will pull the php-5.6 image, and start it.

### SSH into the container (password: secret):
```shell
ssh -p 2222 homestead@localhost
```

### Add a virtual host
Assuming you mapped your apps folder to ```/apps``` (you can change mappings in the docker-compose.yml file, it's prefered to use absolute paths), you can do:
```shell
cd / && sudo ./serve.sh myapp.dev /apps/myapp/public
```
In the host, update ``` /etc/hosts ``` to include your app domain:
```shell
127.0.0.1               myapp.dev
```

### Everything should work - enjoy !!!
Our web container starts nginx, php5-fpm, beanstalk. and has gruntjs, gulp, bower.
some relevant ports have been added to docker-compose.yml ( livereload standard port, karma server port ), change them if you need to.

### Notes
- To connect to the database from homestead, use the name of the container defined in the docker-compose file as the hostname. (eg. mariadb)
- To connect to the database from the host machine, use localhost as the host and port 33060 (or what you defined in the ports).
- The database notes also apply to the redis instance and any other containers that you link.
- Databases: by default mariadb is used as a database, but you are free to use any database you want: choose from these excellent images by Tutum: [tutum/mysql](https://github.com/tutumcloud/mysql) or [tutum/postgresql](https://github.com/tutumcloud/postgresql), they expose different environment variables, so don't forget to update your docker-compose file accordingly.
- For more information, please look at docker compose [website](https://docs.docker.com/compose/)


### TODO
- [x] use supervisor (should monitor: nginx, php-fpm, beanstalk, ssh server)
