FROM ubuntu:14.04
MAINTAINER computerfr33k

ENV DEBIAN_FRONTEND noninteractive

# Copy static ssh host keys (prevents the host key from changing on every build)
COPY ssh_host_rsa_key /etc/ssh/ssh_host_rsa_key
COPY ssh_host_dsa_key /etc/ssh/ssh_host_dsa_key

# Install packages
ADD provision.sh /provision.sh
ADD serve.sh /serve.sh

RUN chmod +x /*.sh

RUN ./provision.sh

COPY supervisor.conf /etc/supervisor/conf.d/supervisor.conf

EXPOSE 80 22 35729 9876

CMD ["/usr/bin/supervisord"]
